var app = new Vue({
    el: '#coupon_form',
    data: {
        firstName: '',
        lastName: '',
        email: '',
        couponCode: ''
    },
    methods: {
        submitForm: function() {
            var data = [ this.firstName, this.lastName, this.email, this.couponCode ];
            fetch( '//API URL HERE', {
                method: 'POST',
                data: data
            })
            .then( // go to thank you page );
        }
    }
})