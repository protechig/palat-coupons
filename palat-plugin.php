<?php
/**
 * Plugin Name: Palat Coupons
 */
add_action('wp_enqueue_scripts', 'palat_coupons_register_scripts');
function palat_coupons_register_scripts() {
   wp_register_script( 'vue', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js', array(), '2.5.16', true );
   wp_register_script( 'palat-vue', plugins_url( '/js/palat-vue.js', __FILE__ ), array('vue', 'wp-api'), '', true);
}
add_shortcode('palat_coupon_form', 'palat_get_coupon_form');
function palat_get_coupon_form() {
    $html = <<<EOT
<div id="coupon_form">
    <form>
        <input v-model="firstName" name="first_name" type="text" />
        <label for="first_name">First Name</label>
        <br>
        <button type="button">Submit</button>
    </form>
</div>

EOT;
    wp_enqueue_script('vue');
    wp_enqueue_script('palat-vue');
    return $html;
}